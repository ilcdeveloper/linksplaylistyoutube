# README #

Este pequeño código escrito en JavaScript sirve para poder descargar el listado de los links de un PlayList de youtube.

El proceso sería el siguiente:

* Abrimos https://www.youtube.com.

* Buscamos el PlayList que deseamos descargar, por ejemplo: https://www.youtube.com/watch?v=1w7OgIMMRc4&list=PLhd1HyMTk3f5yqcPXjLo8qroWJiMMFBSk

* Abrimos el Inspector. Botón derecho>inspeccionar elemento o pulsando F12.

* En la pestaña Console copiamos el código que encontrarémos en el fichero linksPlayListYoutube.js y pulsamos la tecla Enter.